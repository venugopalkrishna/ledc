/*{
   "reactions": {
      "data": [
         {
            "id": "674726762594032",
            "name": "Skylar Leanne Huber",
            "type": "LIKE"
         },
         {
            "id": "1760103464014597",
            "name": "Daniella Mata",
            "type": "LIKE"
         },
         {
            "id": "10154898198570230",
            "name": "Sylvia Pozeg",
            "type": "LIKE"
         },
         {
            "id": "10203478683401524",
            "name": "Eesha Khant",
            "type": "LOVE"
         },
         {
            "id": "366356453513516",
            "name": "Claudia Paguaga",
            "type": "LIKE"
         },
         {
            "id": "10205168300997875",
            "name": "Omayra Toro",
            "type": "LIKE"
         },
         {
            "id": "830319007034372",
            "name": "James Gabriel",
            "type": "LIKE"
         },
         {
            "id": "10153338461958524",
            "name": "Beatriz Salinas Viaud",
            "type": "LIKE"
         },
         {
            "id": "488419584592215",
            "name": "Daysi Recinos",
            "type": "LIKE"
         },
         {
            "id": "10155561685725451",
            "name": "Soumya Bhat",
            "type": "LIKE"
         }
      ],
      "paging": {
         "cursors": {
            "before": "TVRBd01EQXhOekF4TkRReE1ERTBPakUxTURjMU5qRTBNREU2TWpVME1EazJNVFl4TXc9PQZDZD",
            "after": "TlRFNE5qWXdORFV3T2pFMU1EYzBOell3TlRnNk1qVTBNRGsyTVRZAeE13PT0ZD"
         }
      }
   },
   "likes": {
      "data": [
         {
            "name": "Skylar Leanne Huber",
            "id": "674726762594032"
         },
         {
            "name": "Daniella Mata",
            "id": "1760103464014597"
         },
         {
            "name": "Sylvia Pozeg",
            "id": "10154898198570230"
         },
         {
            "name": "Claudia Paguaga",
            "id": "366356453513516"
         },
         {
            "name": "Omayra Toro",
            "id": "10205168300997875"
         },
         {
            "name": "James Gabriel",
            "id": "830319007034372"
         },
         {
            "name": "Beatriz Salinas Viaud",
            "id": "10153338461958524"
         },
         {
            "name": "Daysi Recinos",
            "id": "488419584592215"
         },
         {
            "name": "Soumya Bhat",
            "id": "10155561685725451"
         }
      ],
      "paging": {
         "cursors": {
            "before": "Njc0NzI2NzYyNTk0MDMy",
            "after": "MTAxNTU1NjE2ODU3MjU0NTEZD"
         }
      }
   },
   "created_time": "2017-10-08T15:18:31+0000",
   "id": "2056768267670268"
}


$request = new FacebookRequest(
  $session,
  'GET',
  '/2056768267670268',
  array(
    'fields' => 'backdated_time,reactions,likes{username,name},created_time'
  )
);

$response = $request->execute();
$graphObject = $response->getGraphObject();
/* handle the result */

/*FB.api(
  '/2056768267670268',
  'GET',
  {"fields":"backdated_time,reactions,likes{username,name},created_time"},
  function(response) {
      // Insert your code here
  }
);*/