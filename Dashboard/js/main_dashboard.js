/*
################ FORMATS ##################
-------------------------------------------
*/
console.log("connected");

var 	formatAsPercentage = d3.format("%"),
		formatAsPercentage1Dec = d3.format(".1%"),
		formatAsInteger = d3.format(","),
		fsec = d3.time.format("%S s"),
		fmin = d3.time.format("%M m"),
		fhou = d3.time.format("%H h"),
		fwee = d3.time.format("%a"),
		fdat = d3.time.format("%d d"),
		fmon = d3.time.format("%b")
		;

window.onresize = function(){ location.reload(); }

/*
############# PIE CHART ###################
-------------------------------------------
*/

function dsPieChart(){

	var dataset = [
	      {category: "London CMA", measure: .5},
	      {category: "City of London", measure: .5}
	      ]
	      ;

	var    pieWidth = window.innerWidth,
		    width = pieWidth - 40,
		   height = pieWidth - 40,
		   outerRadius = Math.min(width, height) / 2,
		   innerRadius = outerRadius * .999,
		   // for animation
		   innerRadiusFinal = outerRadius * .5,
		   innerRadiusFinal3 = outerRadius* .47,
		   color = d3.scale.category20()    //built in range of colors
		   ;

		   if (pieWidth >= 640) {
		   	width = pieWidth / 2.25,
		   	height = pieWidth / 2.25,
		   	outerRadius = Math.min(width, height) / 2,
		   	innerRadius = outerRadius * .999,
		   	innerRadiusFinal = outerRadius * .5,
		   	innerRadiusFinal3 = outerRadius * .47;
		   }

		   if (pieWidth >= 1024) {
		   	width = pieWidth / 2.5,
		   	height = pieWidth / 2.5,
		   	outerRadius = Math.min(width, height) / 2.5,
		   	innerRadius = outerRadius * .999,
		   	innerRadiusFinal = outerRadius * .5,
		   	innerRadiusFinal3 = outerRadius * .47;
		   }

	var vis = d3.select("#pieChart")
	     .append("svg:svg")              //create the SVG element inside the <body>
	     .data([dataset])                   //associate our data with the document
	         .attr("width", width)           //set the width and height of our visualization (these will be attributes of the <svg> tag
	         .attr("height", height)
	     		.append("svg:g")                //make a group to hold our pie chart
	         .attr("transform", "translate(" + outerRadius + "," + outerRadius + ")")    //move the center of the pie chart from 0, 0 to radius, radius
				;

   var arc = d3.svg.arc()              //this will create <path> elements for us using arc data
        	.outerRadius(outerRadius).innerRadius(innerRadius);

   // for animation
   var arcFinal = d3.svg.arc().innerRadius(innerRadiusFinal).outerRadius(outerRadius);
	var arcFinal3 = d3.svg.arc().innerRadius(innerRadiusFinal3).outerRadius(outerRadius);

   var pie = d3.layout.pie()           //this will create arc data for us given a list of values
        .value(function(d) { return d.measure; });    //we must tell it out to access the value of each element in our data array

   var arcs = vis.selectAll("g.slice")     //this selects all <g> elements with class slice (there aren't any yet)
        .data(pie)                          //associate the generated pie data (an array of arcs, each having startAngle, endAngle and value properties)
        .enter()                            //this will create <g> elements for every "extra" data element that should be associated with a selection. The result is creating a <g> for every object in the data array
            .append("svg:g")                //create a group to hold each slice (we will have a <path> and a <text> element associated with each slice)
               .attr("class", "slice")    //allow us to style things in the slices (like text)
               .on("mouseover", mouseover)
    				.on("mouseout", mouseout)
    				.on("click", up)
    				;

        arcs.append("svg:path")
               .attr("fill", function(d, i) { return color(i); } ) //set the color for each slice to be chosen from the color function defined above
               .attr("d", arc)     //this creates the actual SVG path using the associated data (pie) with the arc drawing function
					.append("svg:title") //mouseover title showing the figures
				   .text(function(d) { return d.data.category});

        d3.selectAll("g.slice").selectAll("path").transition()
			    .duration(750)
			    .delay(10)
			    .attr("d", arcFinal )
			    ;

	  // Add a label to the larger arcs, translated to the arc centroid and rotated.
	  // source: http://bl.ocks.org/1305337#index.html
	  /*arcs.filter(function(d) { return d.endAngle - d.startAngle > .2; })
	  		.append("svg:text")
	      .attr("dy", ".35em")
	      .attr("text-anchor", "middle")
	      .attr("transform", function(d) { return "translate(" + arcFinal.centroid(d) + ")rotate(" + angle(d) + ")"; })
	      //.text(function(d) { return formatAsPercentage(d.value); })
	      .text(function(d) { return d.data.category; })
	      ;*/

	   // Computes the label angle of an arc, converting from radians to degrees.
		function angle(d) {
		    var a = (d.startAngle + d.endAngle) * 90 / Math.PI - 90;
		    return a > 90 ? a - 180 : a;
		}


		// Pie chart title
		vis.append("svg:text")
	     	.attr("dy", ".35em")
	      .attr("text-anchor", "middle")
	      .text("Select a group")
	      .attr("class","title")
	      ;



	function mouseover() {
	  d3.select(this).select("path").transition()
	      .duration(750)
	        		//.attr("stroke","red")
	        		//.attr("stroke-width", 1.5)
	        		.attr("d", arcFinal3)
	        		;
	}

	function mouseout() {
	  d3.select(this).select("path").transition()
	      .duration(750)
	        		//.attr("stroke","blue")
	        		//.attr("stroke-width", 1.5)
	        		.attr("d", arcFinal)
	        		;
	}

	function up(d, i) {

				/* update bar chart when user selects piece of the pie chart */
				//updateBarChart(dataset[i].category);
				updateBarChart(d.data.category, color(i));
				updateLineChart(d.data.category, color(i));

	}
}

dsPieChart();

/*
############# BAR CHART ###################
-------------------------------------------
*/



var datasetBarChart = [
//All
{ group: "All", category: "01", measure: 0 },
{ group: "All", category: "02", measure: 0 },
{ group: "All", category: "03", measure: 0 },
{ group: "All", category: "04", measure: 0 },
{ group: "All", category: "05", measure: 0 },
{ group: "All", category: "06", measure: 0 },
{ group: "All", category: "07", measure: 0 },
{ group: "All", category: "08", measure: 0 },
{ group: "All", category: "09", measure: 0 },
{ group: "All", category: "10", measure: 0 },
{ group: "All", category: "11", measure: 0 },
{ group: "All", category: "12", measure: 0 },
{ group: "All", category: "13", measure: 0 },
{ group: "All", category: "14", measure: 0 },
{ group: "All", category: "15", measure: 0 },
{ group: "All", category: "16", measure: 0 },

{ group: "London CMA", category: "01", measure: 246 },
{ group: "London CMA", category: "02", measure: 247 },
{ group: "London CMA", category: "03", measure: 255 },
{ group: "London CMA", category: "04", measure: 263 },
{ group: "London CMA", category: "05", measure: 267 },
{ group: "London CMA", category: "06", measure: 268 },
{ group: "London CMA", category: "07", measure: 270 },
{ group: "London CMA", category: "08", measure: 268 },
{ group: "London CMA", category: "09", measure: 265 },
{ group: "London CMA", category: "10", measure: 262 },
{ group: "London CMA", category: "11", measure: 261 },
{ group: "London CMA", category: "12", measure: 265 },
{ group: "London CMA", category: "13", measure: 263 },
{ group: "London CMA", category: "14", measure: 263 },
{ group: "London CMA", category: "15", measure: 270 },
{ group: "London CMA", category: "16", measure: 264 },

{ group: "City of London", category: "01", measure: 180 },
{ group: "City of London", category: "02", measure: 0 },
{ group: "City of London", category: "03", measure: 0 },
{ group: "City of London", category: "04", measure: 0 },
{ group: "City of London", category: "05", measure: 0 },
{ group: "City of London", category: "06", measure: 192 },
{ group: "City of London", category: "07", measure: 0 },
{ group: "City of London", category: "08", measure: 0 },
{ group: "City of London", category: "09", measure: 0 },
{ group: "City of London", category: "10", measure: 0 },
{ group: "City of London", category: "11", measure: 196 },
{ group: "City of London", category: "12", measure: 0 },
{ group: "City of London", category: "13", measure: 0 },
{ group: "City of London", category: "14", measure: 0 },
{ group: "City of London", category: "15", measure: 0 },
{ group: "City of London", category: "16", measure: 0 }
]
;

// set initial group value
var group = "All";

function datasetBarChosen(group) {
	var ds = [];
	for (x in datasetBarChart) {
		 if(datasetBarChart[x].group==group){
		 	ds.push(datasetBarChart[x]);
		 }
		}
	return ds;
}


function dsBarChartBasics() {

		var margin = {top: 30, right: 30, bottom: 50, left: 0},
		width = window.innerWidth - margin.left - margin.right,
	    height = 300 - margin.top - margin.bottom,
		colorBar = d3.scale.category20(),
		barPadding = 1;

		if (window.innerWidth >= 640) {
	    	var margin = {top: 30, right: 30, bottom: 50, left: 30},
	    	width = (window.innerWidth / 2) - margin.left - margin.right,
	    	height = (window.innerWidth / 2) - margin.top - margin.bottom;
	    }

	    if (window.innerWidth >= 1024) {
	    	var width = (window.innerWidth / 3) - margin.left - margin.right,
	    	height = (window.innerWidth / 3) - margin.top - margin.bottom;
	    }

		return {
			margin : margin,
			width : width,
			height : height,
			colorBar : colorBar,
			barPadding : barPadding
		}
		;
}

function dsBarChart() {

	var firstDatasetBarChart = datasetBarChosen(group);

	var basics = dsBarChartBasics();

	var margin = basics.margin,
		width = basics.width,
	   height = basics.height,
		colorBar = basics.colorBar,
		barPadding = basics.barPadding
		;

	var 	xScale = d3.scale.linear()
						.domain([0, firstDatasetBarChart.length])
						.range([0, width])
						;

	// Create linear y scale
	// Purpose: No matter what the data is, the bar should fit into the svg area; bars should not
	// get higher than the svg height. Hence incoming data needs to be scaled to fit into the svg area.
	var yScale = d3.scale.linear()
			// use the max funtion to derive end point of the domain (max value of the dataset)
			// do not use the min value of the dataset as min of the domain as otherwise you will not see the first bar
		   .domain([0, d3.max(firstDatasetBarChart, function(d) { return d.measure; })])
		   // As coordinates are always defined from the top left corner, the y position of the bar
		   // is the svg height minus the data value. So you basically draw the bar starting from the top.
		   // To have the y position calculated by the range function
		   .range([height, 0])
		   ;

	//Create SVG element

	var svg = d3.select("#barChart")
			.append("svg")
		    .attr("width", width + margin.left + margin.right)
		    .attr("height", height + margin.top + margin.bottom)
		    .attr("id","barChartPlot")
		    ;

	var plot = svg
		    .append("g")
		    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
		    ;

	plot.selectAll("rect")
		   .data(firstDatasetBarChart)
		   .enter()
		   .append("rect")
			.attr("x", function(d, i) {
			    return xScale(i);
			})
		   .attr("width", width / firstDatasetBarChart.length - barPadding)
			.attr("y", function(d) {
			    return yScale(d.measure);
			})
			.attr("height", function(d) {
			    return height-yScale(d.measure);
			})
			.attr("fill", "lightgrey")
			;


	// Add y labels to plot

	plot.selectAll("text")
	.data(firstDatasetBarChart)
	.enter()
	.append("text")
	.text(function(d) {
			return formatAsInteger(d3.round(d.measure));
	})
	.attr("text-anchor", "middle")
	// Set x position to the left edge of each bar plus half the bar width
	.attr("x", function(d, i) {
			return (i * (width / firstDatasetBarChart.length)) + ((width / firstDatasetBarChart.length - barPadding) / 2);
	})
	.attr("y", function(d) {
			return yScale(d.measure) + 14;
	})
	.attr("class", "yAxis")
	/* moved to CSS
	.attr("font-family", "sans-serif")
	.attr("font-size", "11px")
	.attr("fill", "white")
	*/
	;

	// Add x labels to chart

	var xLabels = svg
		    .append("g")
		    .attr("transform", "translate(" + margin.left + "," + (margin.top + height)  + ")")
		    ;

	xLabels.selectAll("text.xAxis")
		  .data(firstDatasetBarChart)
		  .enter()
		  .append("text")
		  .text(function(d) { return d.category;})
		  .attr("text-anchor", "middle")
			// Set x position to the left edge of each bar plus half the bar width
						   .attr("x", function(d, i) {
						   		return (i * (width / firstDatasetBarChart.length)) + ((width / firstDatasetBarChart.length - barPadding) / 2);
						   })
		  .attr("y", 15)
		  .attr("class", "xAxis")
		  //.attr("style", "font-size: 12; font-family: Helvetica, sans-serif")
		  ;

	// Title

	svg.append("text")
		.attr("x", (width + margin.left + margin.right)/2)
		.attr("y", 15)
		.attr("class","title")
		.attr("text-anchor", "middle")
		.text("Click to select a group")
		;
}

dsBarChart();

 /* ** UPDATE CHART ** */

/* updates bar chart on request */

function updateBarChart(group, colorChosen) {

		var currentDatasetBarChart = datasetBarChosen(group);

		var basics = dsBarChartBasics();

		var margin = basics.margin,
			width = basics.width,
		   height = basics.height,
			colorBar = basics.colorBar,
			barPadding = basics.barPadding
			;

		var 	xScale = d3.scale.linear()
			.domain([0, currentDatasetBarChart.length])
			.range([0, width])
			;


		var yScale = d3.scale.linear()
	      .domain([0, d3.max(currentDatasetBarChart, function(d) { return d.measure; })])
	      .range([height,0])
	      ;

	   var svg = d3.select("#barChart svg");

	   var plot = d3.select("#barChartPlot")
	   	.datum(currentDatasetBarChart)
		   ;

	  		/* Note that here we only have to select the elements - no more appending! */
	  	plot.selectAll("rect")
	      .data(currentDatasetBarChart)
	      .transition()
			.duration(750)
			.attr("x", function(d, i) {
			    return xScale(i);
			})
		   .attr("width", width / currentDatasetBarChart.length - barPadding)
			.attr("y", function(d) {
			    return yScale(d.measure);
			})
			.attr("height", function(d) {
			    return height-yScale(d.measure);
			})
			.attr("fill", colorChosen)
			;


		plot.selectAll("text.yAxis") // target the text element(s) which has a yAxis class defined
			.data(currentDatasetBarChart)
			.transition()
			.duration(750)
		   .attr("text-anchor", "middle")
		   .attr("x", function(d, i) {
		   		return (i * (width / currentDatasetBarChart.length)) + ((width / currentDatasetBarChart.length - barPadding) / 2);
		   })
		   .attr("y", function(d) {
		   		return yScale(d.measure) + 14;
		   })
		   .text(function(d) {
				return formatAsInteger(d3.round(d.measure));
		   })
		   .attr("class", "yAxis")
		;


		svg.selectAll("text.title") // target the text element(s) which has a title class defined
			.attr("x", (width + margin.left + margin.right)/2)
			.attr("y", 15)
			.attr("class","title")
			.attr("text-anchor", "middle")
			.text(group + "'s Labour Force x 1000 Since 2001")
		;
}


/*
############# LINE CHART ##################
-------------------------------------------
*/

var datasetLineChart = [

{ group: "All", category: "01", measure: 6.2 },
{ group: "All", category: "02", measure: 6.8 },
{ group: "All", category: "03", measure: 6.9 },
{ group: "All", category: "04", measure: 6.1 },
{ group: "All", category: "05", measure: 6.6 },
{ group: "All", category: "06", measure: 6.2 },
{ group: "All", category: "07", measure: 5.8 },
{ group: "All", category: "08", measure: 6.9 },
{ group: "All", category: "09", measure: 9.1 },
{ group: "All", category: "10", measure: 9.5 },
{ group: "All", category: "11", measure: 8.2 },
{ group: "All", category: "12", measure: 8.5 },
{ group: "All", category: "13", measure: 9.2 },
{ group: "All", category: "14", measure: 7.9 },
{ group: "All", category: "15", measure: 6.6 },
{ group: "All", category: "16", measure: 6.6 }
]
;

// set initial category value
var group = "All";

function datasetLineChartChosen(group) {
	var ds = [];
	for (x in datasetLineChart) {
		 if(datasetLineChart[x].group==group){
		 	ds.push(datasetLineChart[x]);
		 }
		}
	return ds;
}

function dsLineChartBasics() {

	var margin = {top: 50, right: 10, bottom: 0, left:10},
	    width = (window.innerWidth - 40) - margin.left - margin.right,
	    height = (window.innerWidth) - margin.top - margin.bottom;

	    if (window.innerWidth >= 640) {
	    	var width = (window.innerWidth - 40) - margin.left - margin.right / 2;
	    	var height = (window.innerWidth - 40) - margin.top - margin.bottom;
	    }

	    if (window.innerWidth >= 1024) {
	    	var width = ((window.innerWidth - 80) - margin.left - margin.right) / 3,
	    	margin = {top: 50, right: 20, bottom: 0, left:10},
	    	height = ((window.innerWidth - 40) - margin.top - margin.bottom) / 2;
	    }

		return {
			margin : margin,
			width : width,
			height : height
		}
		;
}


function dsLineChart() {

	var firstDatasetLineChart = datasetLineChartChosen(group);

	var basics = dsLineChartBasics();

	var margin = basics.margin,
		width = basics.width,
	   height = basics.height
		;

	var xScale = d3.scale.linear()
	    .domain([0, firstDatasetLineChart.length-1])
	    .range([0, width])
	    ;

	var yScale = d3.scale.linear()
	    .domain([0, d3.max(firstDatasetLineChart, function(d) { return d.measure; })])
	    .range([height, 0])
	    ;

	var line = d3.svg.line()
	    //.x(function(d) { return xScale(d.category); })
	    .x(function(d, i) { return xScale(i); })
	    .y(function(d) { return yScale(d.measure); })
	    ;

	var svg = d3.select("#lineChart").append("svg")
	    .datum(firstDatasetLineChart)
	    .attr("width", width + margin.left + margin.right)
	    .attr("height", height + margin.top + margin.bottom)
	    // create group and move it so that margins are respected (space for axis and title)

	var plot = svg
	    .append("g")
	    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
	    .attr("id", "lineChartPlot")
	    ;

		/* descriptive titles as part of plot -- start */
	var dsLength=firstDatasetLineChart.length;

	plot.append("text")
		.text(firstDatasetLineChart[dsLength-1].measure + "% Current")
		.attr("id","lineChartTitle2")
		.attr("x",width/2)
		.attr("y",height/2)
		;
	/* descriptive titles -- end */

	plot.append("path")
	    .attr("class", "line")
	    .attr("d", line)
	    // add color
		.attr("stroke", "maroon")
	    ;

	plot.selectAll(".dot")
	    .data(firstDatasetLineChart)
	  	 .enter().append("circle")
	    .attr("class", "dot")
	    //.attr("stroke", function (d) { return d.measure==datasetMeasureMin ? "red" : (d.measure==datasetMeasureMax ? "green" : "steelblue") } )
	    .attr("fill", "white")
	    //.attr("stroke-width", function (d) { return d.measure==datasetMeasureMin || d.measure==datasetMeasureMax ? "3px" : "1.5px"} )
	    .attr("cx", line.x())
	    .attr("cy", line.y())
	    .attr("r", 6)
	    .attr("stroke", "maroon")
	    .append("title")
	    .text(function(d) { return d.category + ": " + formatAsInteger(d.measure); })
	    ;

	svg.append("text")
		.text("Current Unemployment 'London CMA' %")
		.attr("id","lineChartTitle1")
		.attr("x",margin.left + ((width + margin.right)/2))
		.attr("y", 10)
		;

}

dsLineChart();


 /* ** UPDATE CHART ** */

/* updates bar chart on request */
function updateLineChart(group, colorChosen) {

	var currentDatasetLineChart = datasetLineChartChosen(group);

	var basics = dsLineChartBasics();

	var margin = basics.margin,
		width = basics.width,
	   height = basics.height
		;

	var xScale = d3.scale.linear()
	    .domain([0, currentDatasetLineChart.length-1])
	    .range([0, width])
	    ;

	var yScale = d3.scale.linear()
	    .domain([0, d3.max(currentDatasetLineChart, function(d) { return d.measure; })])
	    .range([height, 0])
	    ;

	var line = d3.svg.line()
    .x(function(d, i) { return xScale(i); })
    .y(function(d) { return yScale(d.measure); })
    ;

   var plot = d3.select("#lineChartPlot")
   	.datum(currentDatasetLineChart)
	   ;

	/* descriptive titles as part of plot -- start */
	var dsLength=currentDatasetLineChart.length;

	plot.select("text")
		.text(currentDatasetLineChart[dsLength-1].measure)
		;
	/* descriptive titles -- end */

	plot
	.select("path")
		.transition()
		.duration(750)
	   .attr("class", "line")
	   .attr("d", line)
	   // add color
		.attr("stroke", colorChosen)
	   ;

	var path = plot
		.selectAll(".dot")
	   .data(currentDatasetLineChart)
	   .transition()
		.duration(750)
	   .attr("class", "dot")
	   .attr("fill", "white")
	   .attr("cx", line.x())
	   .attr("cy", line.y())
	   .attr("r", 3.5)
	   // add color
		.attr("stroke", colorChosen)
	   ;

	   path
	   .selectAll("title")
	   .text(function(d) { return d.category + ": " + formatAsInteger(d.measure); })
	   ;

}
