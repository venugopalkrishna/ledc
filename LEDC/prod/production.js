var myApp = {
  mainMessage : "Welcome!",

  mainGreeting(){
    console.log("Welcome to ledc!");
  }
}

myApp.module1 = {
  saySomething(message)
  {
    console.log(myApp.mainMessage, message, "Second module");
  },

  doSomething(){
    console.log('module1 doSomething function');
  }
}

myApp.module2 = {
  doSomething()
  {
    console.log('module1 doSomething function');
  }
}

(() =>{
  myApp.mainGreeting();

  myApp.module1.saySomething("LEDC Website");

  function myFunction()
  {
    // var theHeading = doument.querySelector('h1');
    //
    // theHeading.textContent = myApp.mainMessage;
  }

  myFunction();
});
